# Multivariable biostatistical model based on in vitro methods for prediction of anthelmintic resistance in goats

journal publication in Data in Brief, a 
> multi-disciplinary open access journal. Data in Brief articles are a  
> fantastic way to describe supplementary data and associated  
> metadata, or full raw datasets deposited in an external repository,  
